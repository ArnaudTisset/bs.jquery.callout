// app.js
// in your requirejs config
requirejs.config({
    baseUrl: '../libs',
    paths: {
        'callout': '../src/app',
        jquery: [
            // 'https://code.jquery.com/jquery-3.1.0.slim.min', // no easing function 
            'https://code.jquery.com/jquery-3.1.0.min.js',
            'jquery-2.2.1.min'
        ]
    }
});

//Start loading the main app file. Put all of your application logic in there.
requirejs(['jquery', 'callout/main'], function($, callout) {
    $(".bs-callout").callout();
});