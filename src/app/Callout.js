// Callout.js
// Callout specific module logic
define(['jquery'], function ()
{
    var callout_type_class  = function(element) {
        var types =["bs-callout-default", "bs-callout-primary", "bs-callout-success", "bs-callout-info", "bs-callout-warning", "bs-callout-danger"],
            classList = element.className.split(/\s+/);


        for (var ii = 0; ii < classList.length; ii++) {
            if ($.inArray( classList[ii], types) > -1) {
                return classList[ii];   
            }
        }
        return  types[0];
        
    }

    $.fn.callout = function(settings) {
        settings = $.extend({}, $.callout.defaults, settings);

        this.each(function() {
            var type  = callout_type_class(this);
                title = $(this).attr('callout-title') || "",
                settings = $.extend({}, settings, { is_close_button: $(this).attr('callout-close') || false} );
                _html = Callout.init(title, $(this).text(), type, settings);

            /// Caution: The .replaceWith() method removes all data and event 
            /// handlers associated with the removed nodes.
            $(this).replaceWith(_html);
        });
    };


    $.callout = function(title, content, type, settings) {
        settings = $.extend({}, $.callout.defaults, settings);
        if (!title || !content) { return ''; }
        return callout.init(title, content, type, settings);
    };

    $.callout.defaults = {
        // tpl: html template for callout
        tpl: "<div class=\"bs-callout\">{{ content }}</div>",
        // tpl: html template for callout title
        tpl_title: "<h4 id=\"{{ title-slug }}\"><a href=\"#{{ title-slug }}\" class=\"headerlink\" title=\"{{ title }}\"></a>{{ title }}</h4>",
        // tpl: html template for callout button
        tpl_button:"<br/><a href=#>{{ close }}</a>",
        // set to true if want to add an nifty close button
        is_close_button: false,
        // the close button label
        button_label: "OK",
        // a timer delay
        delay: 5000
    }

    return {

        /*
         * Main entry point of the App
         * 
         * @param title
         * @param content
         * @param type
         * @param settings
         */
        init: function(title, content, type, settings) {
            var element = $(settings.tpl)
                              .text(content)
                              .addClass(type);

            title = $(settings.tpl_title)
                        .attr('id', slugize(title))
                        .text(title);

            element.prepend( title )
                   .append( ( settings.is_close_button ? $(settings.tpl_button).text(settings.button_label).on('click', Callout.click) : "" ) );
                   

            if(settings.is_close_button) {
                var timer = setTimeout(function() {
                    $(element).fadeOut("slow", function(object) {
                        $(element).remove();
                    });
                    clearTimeout(timer);
                }, 5000);
            }

            return element;
        },

        click : function(event) {
            if (event.target !== this)
                return '';

            $(this).closest('div').fadeOut('slow', function() {
                 $(this).remove();
            });
        }
    }
});