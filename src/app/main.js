// main.js
define(function (require, module) {
    // Load any app-specific modules
    // with a relative require call
    module.exports = {
        escape_diacritic: require('./escape_diacritic'),
        escape_regexp: require('./escape_regexp'),
        slugize: require('./slugize'),
    };



    Callout = require('./Callout');
    return Callout;
});